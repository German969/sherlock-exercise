const bodyParser = require('body-parser');
const express = require('express');
const moment = require('moment');
const validate = require('./validator');

const app = express();
const PORT = 4000;

app.use(bodyParser.json());

app.post('/validate', function(req, res) {
  const newValidation = {
    dateTime: moment().format('MM/DD/YYYY HH:MMA'),
    input: req.body.string,
    output: validate(req.body.string).output,
    valid: validate(req.body.string).valid
  };

  res.json(newValidation);
});

app.listen(PORT, function() {
  console.log("Server is running on Port: " + PORT);
});