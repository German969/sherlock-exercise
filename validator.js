const findOccurrences = (array) => {
  return array.reduce((result, current) => {
    result[current] ? result[current]++ : result[current] = 1;

    return result;
  }, {});
};

const areAllValuesEqual = (array) => {
  return array.every( value => value === array[0] );
};

const isValidByDeletingOne = (element, index, [...array]) => {
  array.splice(index, 1);

  const oneOccurrenceDifference = (
      array.length === 0 || element === 1
  );

  return oneOccurrenceDifference && areAllValuesEqual(array);
};

module.exports = function (stringToValidate) {
  const characters = Array.from(stringToValidate);
  const occurrencesByCharacter = findOccurrences(characters);
  const occurrencesOnly = Object.values(occurrencesByCharacter);

  const occurrenceIndexToDelete = occurrencesOnly.findIndex(isValidByDeletingOne);
  const elementToDelete = Object.keys(occurrencesByCharacter)[occurrenceIndexToDelete];
  const elementIndexToDelete = characters.findIndex((element) => {
    return elementToDelete === element
  });

  if (elementIndexToDelete > -1 && !areAllValuesEqual(occurrencesOnly)) {
    characters.splice(elementIndexToDelete, 1);
  }

  return {
    valid: elementIndexToDelete > -1 || areAllValuesEqual(occurrencesOnly),
    output: characters.join("")
  };
};