import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import useValidateForm from './use-validate-form';
import React from 'react';
import './validate-form.css';

function ValidateForm() {
  const {string, handleInputChange, handleSubmit} = useValidateForm();

  return (
      <Form className={"validate-form"} onSubmit={handleSubmit}>
        <Form.Row>
          <Col>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>String to validate</Form.Label>
              <Form.Control
                  type="text"
                  placeholder="Enter string"
                  onChange={handleInputChange}
                  value={string}
              />
            </Form.Group>
          </Col>
          <Col className={"submit-column"}>
            <Form.Group controlId="formBasicEmail">
              <Button variant="primary" type="submit">
                Validate
              </Button>
            </Form.Group>
          </Col>
        </Form.Row>
      </Form>
  );
}

export default ValidateForm;