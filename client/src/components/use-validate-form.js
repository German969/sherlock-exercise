import axios from 'axios';
import {useState} from 'react';

const useValidateForm = () => {
  const [string, setString] = useState("");

  const handleValidationComplete = (result) => {
    const validationHistory = JSON.parse(localStorage.getItem('validationHistory')) || [];

    if (validationHistory) {
      validationHistory.push(result);
    }

    localStorage.setItem('validationHistory', JSON.stringify(validationHistory));
  };

  const handleSubmit = (event) => {
    if (event) {
      axios
      .post("/validate", {string: string})
      .then(res => handleValidationComplete(res.data))
      .catch(err => console.log(err));
    }
  };

  const handleInputChange = (event) => {
    event.persist();
    setString(event.target.value);
  };

  return {
    handleSubmit,
    handleInputChange,
    string
  };
};

export default useValidateForm;