import React from 'react';
import Table from 'react-bootstrap/Table';

function HistoryTable(props) {

  const renderHistoryValues = function () {
    return props.validations.map((currentValidation, index) => {
      return (
          <tr key={index}>
            <td>{currentValidation.dateTime}</td>
            <td>{currentValidation.input}</td>
            <td>{currentValidation.output}</td>
            <td>{currentValidation.valid ? 'YES': 'NO'}</td>
          </tr>
      );
    })
  };

  return (
      <Table striped bordered>
        <thead>
        <tr>
          <th>Datetime</th>
          <th>Input</th>
          <th>Output</th>
          <th>Valid</th>
        </tr>
        </thead>
        <tbody>
        {renderHistoryValues()}
        </tbody>
      </Table>
  );
}

export default HistoryTable;