import HistoryTable from './components/history-table';
import React from 'react';
import ValidateForm from './components/validate-form';
import './App.css';

function App() {
  const getValidationsFromStore = () => {
    return JSON.parse(localStorage.getItem('validationHistory')) || [];
  };

  return (
    <div className="App">
      <h1>Sherlock and the Valid String</h1>
      <ValidateForm/>
      <HistoryTable validations={getValidationsFromStore()}/>
    </div>
  );
}

export default App;
